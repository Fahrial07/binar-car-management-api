const adminRepository = require("../repositories/adminRepository");

module.exports = {

    async create(data){
        return adminRepository.create(data)
    },

    async list(){
        try {
            const admin = await adminRepository.findAllAdmin();
            return {
                data: admin
            }
        } catch (error) {
            throw error;
        }
    },

    async member()
    {
        const member = await adminRepository.findMember();
        return {
            data: member
        }
    },

    async update(id, requestBody)
    {
        return adminRepository.update(id, requestBody);
    },

    async delete(id)
    {
        return adminRepository.delete(id);
    }

};