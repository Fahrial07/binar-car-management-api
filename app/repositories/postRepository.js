const { User } = require("../models");
const bcrypt = require("bcrypt");
module.exports = {
  create(data) {

    const passwordHash = bcrypt.hashSync(data.password, 10);
    const defautlValue = "admin";

    data.password = passwordHash;
    data.role = defautlValue;

    return User.create(data);
  },

  // update(id, updateArgs) {
  //   return Post.update(updateArgs, {
  //     where: {
  //       id,
  //     },
  //   });
  // },

  // delete(id) {
  //   return Post.destroy(id);
  // },

  // find(id) {
  //   return Post.findByPk(id);
  // },

  // findAll() {
  //   return Post.findAll();
  // },

  // getTotalPost() {
  //   return Post.count();
  // },
};
