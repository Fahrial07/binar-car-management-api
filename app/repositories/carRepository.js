const { car } = require('../models');

module.exports = {
    create(data)
    {
        return car.create(
            data,
            {
                paranoid: false
            }
        );
    },

    findAllCar()
    {
        return car.findAll();
    },

    carUpdate(id, requestBody, requestFile, user)
    {
        if(requestFile == null){

            return car.update(requestBody, requestFile,
            {
                name: requestBody.name,
                plate: requestBody.plate,
                price: requestBody.price,
                capacity: requestBody.capacity,
                description: requestBody.description,
                updatedBy: user.id,
                where: {
                    id
                },
                paranoid: false,
                timestamps:false
            });

        }else{

            return car.update(requestBody, requestFile, user,
            {
                name: requestBody.name,
                plate: requestBody.plate,
                image: requestFile.filename,
                capacity: requestBody.capacity,
                description: requestBody.description,
                updatedBy: user.id,
                where: {
                    id
                },
                paranoid: false,
                timestamps:false
            });


        }
    },

    carDelete(id, userId)
    {
        return car.destroy({
            where: {
                id
            },
            timestamps: true,
            paranoid:true
        })
        .then( (deleted) => {
         return car.update({
                where: {
                    id
                }
            })
        })
    }


};