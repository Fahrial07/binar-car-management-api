// const { Error } = require("sequelize/types");
const authRepository = require("../repositories/authRepository");
// const postRepository = require("../repositories/postRepository");
const jwt = require("jsonwebtoken");
const bcrypt = require("bcrypt");
const { whoami } = require("../controllers/api/v1/authController");

module.exports = {

    async register(data)
    {
        return authRepository.register(data);
    },

    async login(data)
    {
        data.email = data.email.toLowerCase();

        const user = await authRepository.findByEmail(data.email);
        // console.log(user);
        if(!user){
            throw new Error("Email tidak di temukan !");
        }

        const isPasswordCorrect = await this.checkPassword(
            user.password,
            data.password
        );
        // console.log(isPasswordCorrect);

        if(!isPasswordCorrect){
            throw new Error("Password salah !");
        }

        const Token = await this.createToken(
            {
                id: user.id,
                email: user.email,
                email: data.email,
                password: data.password,
                role: user.role
            }
        );

        return {
            id: user.id,
            name: user.name,
            email: data.email,
            role: user.role,
            Token
        }

    },

    async currentUser(req)
    {
        const bearerToken = req.authorization;
        const token = bearerToken.split('Bearer ')[1];
        const payload = jwt.verify(token, process.env.JWT_SECRET);
        const user = await authRepository.currentUser(payload.id);
        return user;
    },


    async checkPassword(encryptedPassword, bodyPassword)
    {
        return bcrypt.compare(
            bodyPassword,
            encryptedPassword
        );
    },

    async createToken(data)
    {
        return jwt.sign(data, process.env.JWT_SECRET)
    },

}