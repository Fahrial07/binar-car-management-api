const { User } = require("../models");
const bcrypt = require("bcrypt");
const { user } = require("pg/lib/defaults");

module.exports = {

  register(data) {

    const passwordHash = bcrypt.hashSync(data.password, 10);
    const defaultValueRole = "member";

    data.password = passwordHash;
    data.role = defaultValueRole;

    return User.create(data, {
      paranoid: false
    });
  },

  findByEmail(email)
  {
    return User.findOne(
      {
        where: { email }
      }
    );
  },

  findById(id)
  {
    return User.findByPk(id);
  },

  currentUser(id)
  {
    return User.findByPk(id);
  }

};