const carRepository = require('../repositories/carRepository');

module.exports = {

    async create(data)
    {
        try {
            return carRepository.create(data);
        } catch (error) {
            throw error;
        }
    },

    async list()
    {
        try {
            const car = await carRepository.findAllCar();
            return {
                data: car,
            }
        } catch (error) {
            throw error;
        }
    },

    async update(id, requestBody, requestFile, user)
    {
        try {
            return await carRepository.carUpdate(id, requestBody, requestFile, user);
        } catch (error) {
            throw error;
        }
    },

    async delete(id, userId)
    {
        try {
            return carRepository.carDelete(id, userId);
        } catch (error) {
            throw error;
        }
    }


};