const adminService = require("../../../services/adminService");

module.exports = {

        create(req, res){
            adminService
            .create(req.body)
            .then(( data ) => {
                res.status(200).json({
                    status: "OK",
                    data
                });
            })
            .catch((err) => {
                res.status(400).json({
                    status: "FAIL",
                    message: err.message,
                });
            })
        },

    list(req, res){
        adminService
        .list()
        .then(( data ) => {
            res.status(200).json({
                status: "OK",
                data
            });
        })
        .catch((err) => {
            res.status(400).json({
                status: "FAIL",
                message: err.message,
            })
        })
    },

    member(req, res){
        adminService
        .member(req.params.id)
        .then(( data ) => {
            res.status(200).json({
                status: "OK",
                data
            });
        })
        .catch((err) => {
            res.status(400).json({
                status: "FAIL",
                message: err.message,
            })
        })
    },

    update(req, res){
        adminService
        .update(req.params.id, req.body)
        .then(( data ) => {
            res.status(200).json({
                status: "OK",
                data
            });
        })
        .catch((err) => {
            res.status(422).json({
                status: "FAIL",
                message: err.message
            });
        })
    },

    delete(req, res){
        adminService
        .delete(req.params.id)
        .then(( data )  => {
            res.status(200).json({
                status: "Deleted",
                data
            });
        })
        .catch((err) => {
            res.status(400).json({
                status: "FAIL",
                message: err.message,
            })
        })
    }

};