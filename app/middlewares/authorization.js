const jwt = require("jsonwebtoken");
const authRepository = require('../repositories/authRepository');

module.exports = {
    async checkToken(req, res, next) {
        try {
            const bearerToken = req.headers.authorization
            const token = bearerToken.split('Bearer ')[1];
            const payload = jwt.verify(token, process.env.JWT_SECRET);
            req.user = await authRepository.findByEmail(payload.email);
            next();
        } catch (error) {
            res.status(401).json({
                status: "Unauthorized",
                message: error.message,
              });
        }
    },
    async checkAdmin(req, res, next) {
        try {
            const bearerToken = req.headers.authorization
            const token = bearerToken.split('Bearer ')[1];
            const payload = jwt.verify(token, process.env.JWT_SECRET);
            if (payload.role === 'admin') {
                req.user = await authRepository.findByEmail(payload.email);
                next();
            } else {
                res.status(401).json({
                    status: "Unauthorized",
                    message: "You are not authorized to access this resource",
                    });
            }
        } catch (error) {
            res.status(401).json({
                status: "Unauthorized",
                message: error.message,
            })
        }
    },

    async checkSuperadmin(req, res, next) {
        try {
            const bearerToken = req.headers.authorization
            const token = bearerToken.split('Bearer ')[1];
            const payload = jwt.verify(token, process.env.JWT_SECRET);
            if (payload.role === 'superadmin') {
                req.user = await authRepository.findByEmail(payload.email);
                next();
            } else {
                res.status(401).json({
                    status: "Unauthorized",
                    message: "You are not authorized to access this resource",
                    });
            }
        } catch (error) {
            res.status(401).json({
                status: "Unauthorized",
                message: error.message,
            })
        }
    },


     async checkAdministrator(req, res, next) {
        try {
            const bearerToken = req.headers.authorization
            const token = bearerToken.split('Bearer ')[1];
            const payload = jwt.verify(token, process.env.JWT_SECRET);
            if (payload.role === 'superadmin' || payload.role === 'admin') {
                req.user = await authRepository.findByEmail(payload.email);
                next();
            } else {
                res.status(401).json({
                    status: "Unauthorized",
                    message: "You are not authorized to access this resource",
                    });
            }
        } catch (error) {
            res.status(401).json({
                status: "Unauthorized",
                message: error.message,
            })
        }
    },

     async checkMember(req, res, next) {
        try {
            const bearerToken = req.headers.authorization
            const token = bearerToken.split('Bearer ')[1];
            const payload = jwt.verify(token, process.env.JWT_SECRET);
            if (payload.role === 'member') {
                req.user = await authRepository.findByEmail(payload.email);
                next();
            } else {
                res.status(401).json({
                    status: "Unauthorized",
                    message: "You are not authorized to access this resource",
                    });
            }
        } catch (error) {
            res.status(401).json({
                status: "Unauthorized",
                message: error.message,
            })
        }
    },


};