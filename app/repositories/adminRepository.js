const { User } = require("../models");
const bcrypt = require("bcrypt");
const { Op } = require("sequelize");
options = { multi: true };

module.exports = {
  create(data) {

    const passwordHash = bcrypt.hashSync(data.password, 10);
    const defautlValue = "admin";

    data.password = passwordHash;
    data.role = defautlValue;

    return User.create(data, {
      paranoid: false
    });
  },

  findAllAdmin(){
    return User.findAll(
      {
        where:{
          role: "admin"
        },
        timestamps: false,
        paranoid: true,
      }
    );
  },

  findMember()
  {
    return User.findAll(
      {
        where:{
          role: "member"
        }
      });
  },

  update(id, requestBody)
  {
    if(requestBody.password == null){
        return User.update(requestBody, {
          where: {
            id: id
          },
          paranoid: false
        });
    } else {
       const  newPassword = bcrypt.hashSync(requestBody.password, 10);
        requestBody.password = newPassword;
        return User.update(requestBody, {
          name: requestBody.name,
          email: requestBody.email,
          password: requestBody.password,
          where: {
            id: id
          },
          paranoid: false
        });
    }
  },

  delete(id)
  {
    return User.destroy(
      {
        where: {
          id: id
        },
        paranoid: true
      });
  }

}