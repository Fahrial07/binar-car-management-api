const carService = require("../../../services/carService");

module.exports = {

    create(req, res) {
        carService
        .create({
            name: req.body.name,
            plate: req.body.plate,
            image: req.file.filename,
            price: req.body.price,
            capacity: req.body.capacity,
            description: req.body.description,
            createdBy: req.user.id
        })
        .then(( data ) => {
            res.status(200).json({
                status: "OK",
                data,
            });
        })
        .catch((err) => {
            res.status(400).json({
                status: "FAIL",
                message: err.message,
            });
        })
    },

    list(req, res){
        carService
        .list()
        .then((data) => {
            res.status(200).json({
                status: "OK",
                data
            });
        })
        .catch((err) => {
            res.status(400).json({
                status: "FAIL",
                message: err.message,
            })
        })
    },

    update(req, res){
            carService
            .update(req.params.id, req.body, req.file, req.user)
            .then((data) => {
                res.status(200).json({
                    status: "OK",
                    data,
                });
            })
            .catch((err) => {
                res.status(400).json({
                    status: "FAIL",
                    message: err.message
                });
            })
    },

    delete(req, res){
        carService
        .delete(
            req.params.id, req.user.id)
        .then((data) => {
            res.status(200).json({
                status: "Car has deleted !",
                data
            });
        })
        .catch((err) => {
            res.status(400).json({
                status: "FAIL",
                message: err.message,
            })
        })
    }

};