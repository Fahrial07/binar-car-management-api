const express = require("express");
const controllers = require("../app/controllers");
const authorization = require("../app/middlewares/authorization");
const apiRouter = express.Router();
const YAML = require("yamljs");
const swaggerUI = require("swagger-ui-express");
const swaggerDocument = YAML.load("./documentation.yaml");

const path = require("path");
const multer = require("multer");

//upload gambar
const storage = multer.diskStorage({
    //destination
    destination: function (req, file, cb) {
            cb(null, path.join(__dirname, "../app/public/img"))
        },

        filename: (req, file, cb) => {
        const fileName = `${Date.now()}${file.originalname.toLowerCase().split(' ').join('-')}`;
        //callback
            cb(null, fileName);
        }
    })
let uploadImage = multer({storage: storage});

/**
 * TODO: Implement your own API
 *       implementations
 */
// apiRouter.get("/api/v1/memberCreate", controllers.api.v1.postController.create);

//auth route
apiRouter.post("/api/v1/register", controllers.api.v1.authController.register);
apiRouter.post("/api/v1/login", controllers.api.v1.authController.login);

//whoami
apiRouter.get("/api/v1/currentUser", authorization.checkToken, controllers.api.v1.authController.currentUser);

//admin route
apiRouter.post("/api/v1/create", authorization.checkSuperadmin, controllers.api.v1.adminController.create);
apiRouter.get("/api/v1/list", authorization.checkSuperadmin, controllers.api.v1.adminController.list);
apiRouter.put("/api/v1/update/:id", authorization.checkSuperadmin, controllers.api.v1.adminController.update)
apiRouter.delete("/api/v1/delete/:id", authorization.checkSuperadmin, controllers.api.v1.adminController.delete)

//list member
apiRouter.get("/api/v1/member", authorization.checkAdministrator, controllers.api.v1.adminController.member);
//car route
apiRouter.post("/api/v1/carCreate", authorization.checkAdministrator, uploadImage.single("image"), controllers.api.v1.carController.create);
apiRouter.get("/api/v1/carList", authorization.checkToken, controllers.api.v1.carController.list);
apiRouter.put("/api/v1/carUpdate/:id", authorization.checkAdministrator, uploadImage.single("image"), controllers.api.v1.carController.update);
apiRouter.delete("/api/v1/carDelete/:id", authorization.checkAdministrator, controllers.api.v1.carController.delete);


//documentation
apiRouter.use("/api/v1/doc", swaggerUI.serve, swaggerUI.setup(swaggerDocument));

/**
 * TODO: Delete this, this is just a demonstration of
 *       error handler
 */


apiRouter.use(controllers.api.main.onLost);
apiRouter.use(controllers.api.main.onError);

module.exports = apiRouter;
